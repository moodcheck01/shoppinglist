//
//  InterfaceController.swift
//  Shopping List Watch WatchKit Extension
//
//  Created by Alexandru Cojocaru on 15/05/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

struct ListItem {
    var title: String
    var isSelected: Bool
}

class InterfaceController: WKInterfaceController {

    let session = WCSession.default

    @IBOutlet weak var table: WKInterfaceTable!

    var dataSource:[ListItem] = [ListItem(title: "Mere golden", isSelected: false),
                                 ListItem(title: "Branza", isSelected: false),
                                 ListItem(title: "Rosii", isSelected: false),
                                 ListItem(title: "Paine", isSelected: false),
                                 ListItem(title: "Apa", isSelected: false)
                                ]
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)

        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }

        table.setNumberOfRows(dataSource.count, withRowType: "ShoppingListRow")

        for (index, item) in dataSource.enumerated() {
            guard let row = table.rowController(at: index) as? ShoppingListRow else { continue }
            row.titleLabel.setText(item.title)
            row.checkMarkIcon.setHidden(!item.isSelected)
        }
    }

    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        guard let row = table.rowController(at: rowIndex) as? ShoppingListRow else { return }
        let newValue = !dataSource[rowIndex].isSelected
        dataSource[rowIndex].isSelected = newValue
        row.checkMarkIcon.setHidden(!newValue)
    }

    @IBAction func handleLongPress(_ sender: Any) {
       
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}

extension InterfaceController: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("received data: \(message)")
        if let value = message["MyShoppingList"] as? Data {
            print(value)
        }
    }
}
