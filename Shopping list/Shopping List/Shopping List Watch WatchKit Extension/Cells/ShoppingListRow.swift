//
//  ShoppingListRow.swift
//  Shopping List Watch WatchKit Extension
//
//  Created by Alexandru Cojocaru on 15/05/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import WatchKit

class ShoppingListRow: NSObject {
    @IBOutlet weak var titleLabel: WKInterfaceLabel!
    @IBOutlet weak var checkMarkIcon: WKInterfaceImage!
    
}
