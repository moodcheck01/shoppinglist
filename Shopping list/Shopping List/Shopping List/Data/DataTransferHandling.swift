//
//  DataTransferHandling.swift
//  Shopping List
//
//  Created by Alexandru Cojocaru on 24/06/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import UIKit
import WatchConnectivity

class DataTransferHandling: NSObject {

    static let shared = DataTransferHandling()
    private var session: WCSession?

    override init() {
        super.init()
        if WCSession.isSupported() {
            session = WCSession.default
            session?.delegate = self
            session?.activate()
        } else {
            session = nil
        }
    }

    func sendItems(items: [Item]){
        if session!.isPaired {
            let data: [String: Any] = ["MyShoppingList": items as Any]
            session?.sendMessage(data, replyHandler: nil, errorHandler: nil)
        }
    }
}

extension DataTransferHandling: WCSessionDelegate {
    func sessionDidBecomeInactive(_ session: WCSession) {
    }

    func sessionDidDeactivate(_ session: WCSession) {
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            if let value = message["watch"] as? String {
                print(value)
            }
        }
    }
}
