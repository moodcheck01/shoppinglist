//
//  ItemTableViewCell.swift
//  Shopping List
//
//  Created by Alexandru Cojocaru on 24/06/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    var title: String? {
        didSet {
            guard let unwrappedTitle = title else { return }
            titleLabel.text = unwrappedTitle
        }
    }

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpUI()
    }

    fileprivate func setUpUI() {
        selectionStyle = .none
        cardView.roundCorners()
        cardView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
