//
//  HomeViewModel.swift
//  Shopping List
//
//  Created by Alexandru Cojocaru on 24/06/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import Foundation

class HomeViewModel: NSObject {
    weak var dataSource: HomeViewDataSource?
    var dataSourceDidChange: (() -> Void)?

    init(dataSource: HomeViewDataSource) {
        self.dataSource = dataSource
    }

    func addNewItem(item: Item) {
        dataSource?.items.append(item)
        dataSourceDidChange?()
    }

    func sendDataToWatch() {
        DataTransferHandling.shared.sendItems(items: dataSource?.items ?? [])
    }
}
