//
//  ViewController.swift
//  Shopping List
//
//  Created by Alexandru Cojocaru on 15/05/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import UIKit

protocol AddNewItemProtocol: class {
    func addNewIem(item: Item)
}

class HomeViewController: UIViewController {

    // MARK: Properties
    private let dataSource = HomeViewDataSource()
    private lazy var viewModel: HomeViewModel = {
        let model = HomeViewModel(dataSource: dataSource)
        model.dataSourceDidChange = { [weak self] in
            self?.tableView.reloadData()
        }

        return model
    }()
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        registerCells()
    }

    // MARK: Functions
    fileprivate func setUpUI() {
        self.view.backgroundColor = .white
        self.title = "Shopping list"
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAdd))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(handleDone))

        navigationItem.setRightBarButtonItems([addButton, doneButton], animated: false)

        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }

    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "itemTableViewCell")
    }

    @objc func handleAdd() {
        let controller = NewItemViewController()
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }

    @objc func handleDone() {
        viewModel.sendDataToWatch()
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO
    }
}

extension HomeViewController: AddNewItemProtocol {
    func addNewIem(item: Item) {
        viewModel.addNewItem(item: item)
    }
}
