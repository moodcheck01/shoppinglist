//
//  NewItemViewController.swift
//  Shopping List
//
//  Created by Alexandru Cojocaru on 24/06/2020.
//  Copyright © 2020 Alex Cojocaru. All rights reserved.
//

import UIKit

class NewItemViewController: UIViewController {

    // MARK: Properties
    weak var delegate: AddNewItemProtocol?

    let titleTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.textAlignment = .center
        textField.setUnderLine()

        return textField
    }()

    lazy var okButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("OK", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.roundCorners()
        button.layer.borderColor = UIColor.darkGray.cgColor
        button.layer.borderWidth = 0.5
        button.addTarget(self, action: #selector(handleOk), for: .touchUpInside)

        return button
    }()

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        titleTextField.becomeFirstResponder()
    }

    // MARK: Functions
    fileprivate func setUpUI() {
        view.backgroundColor = .white
        view.addSubview(titleTextField)
        view.addSubview(okButton)

        titleTextField.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: -50).isActive = true
        titleTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 30).isActive = true
        titleTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30).isActive = true
        titleTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true

        okButton.topAnchor.constraint(equalTo: titleTextField.bottomAnchor, constant: 20).isActive = true
        okButton.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        okButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        okButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }

    @objc func handleOk() {
        let item = Item(title: titleTextField.text ?? "")
        delegate?.addNewIem(item: item)
        titleTextField.text = ""
    }
}
